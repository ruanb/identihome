// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User       		= require('../app/models/user');

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

 	// =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {

		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

				// if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
                newUser.name = req.body.name;
                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password); // use the generateHash function in our user model
                // newUser.pets[0].petName = req.body.petName;
                // newUser.pets[0].chipNr = req.body.chipNr;
                newUser.contact = req.body.contact;
                newUser.address = req.body.address;
                newUser.postalCode = req.body.postalCode;
                newUser.alternateContact = req.body.alternateContact;
                newUser.promotions = req.body.promotions;
                newUser.registered = new Date().getTime();
                console.log(newUser);
                // return;

                // var name = req.body.name;
                var welcomeEmail = {
                    from: 'info@vetsbrands.co.za',
                    to: email,
                    // to: 'ruan.botha.live@gmail.com',
                    subject: 'Welcome to IdentiHome: ' + req.body.name,
                    html: 
                    "Dear " + req.body.name + ", <br><br>"
                    + "Welcome to <b>IdentiHome</b>. Thank you for your registering with us. <br><br>" 
                    + "Feel free to register any pet by its chip number on our site, to save your pet's details on our central database. <br>"
                    + "This allows veterinarians in your area to scan and search for your pet's chip number if your pet was lost. "
                    + "Your details will then be provided to the veterinarian, allowing him/her to contact you immediately and notify you "
                    + "that they have safely found your pet."
                    + "<br><br>"

                    // + "He/she provided the following details:" + "</p><p>" 
                    // + "Searcher's Name: " + name + "<br>"
                    // + "Searcher's contact number: " + contact + "<br>"
                    // + "Clinic / Welfare: " + clinic + "<br><br>"

                    + "Feel free to contact IdentiHome admin (0764032571) " 
                    + "if you ever have any queries about this process."
                    + "<br><br>" + "Kind regards, " + "<br>" + "IdentiHome Staff"
                    + "<br>" + "(0764032571)"
                };
                
                var newSignup = {
                    from: 'info@vetsbrands.co.za',
                    to: 'markobotha@gmail.com, ruan.botha.live@gmail.com, info@vetsbrands.co.za',
                    // to: 'ruan.botha.live@gmail.com',
                    subject: 'ATTENTION: New IdentiHome Signup: ' + req.body.name,
                    html: 'ATTENTION: New IdentiHome Signup: ' + req.body.name
                };

				// save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;

                    // Send email about new user signup
                    sendmail(welcomeEmail);
                    sendmail(newSignup);

                    return done(null, newUser);
                });
            }

        });

    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });

    }));

};
