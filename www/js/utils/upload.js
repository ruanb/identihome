/*****************************************************
Please note that this file may have been modified, changed and/or adapted from the original.
This notice serves to fulfill the requirement of the Apache 2.0 license clause 4(b): 
"You must cause any modified files to carry prominent notices stating that You changed the files."
Please see the licenses section to find the license to the original.
*****************************************************/

// $('#fileInput').change(function(e){
$('.fileInput').change(function(e){
	$(e.currentTarget).attr('disabled', true);
	$(e.currentTarget).attr('id', 'fileInput'); //set id to fileInput
	var parent = $(e.currentTarget).parent();
	var chipNr = $(parent).find('.chipNr').text();

	var s3upload = new S3Upload({
		file_dom_selector: 'fileInput',
		s3_object_name: chipNr + '.jpg',
		s3_sign_put_url: '/signS3/image/pet',
		onProgress: function(percent, message){
			console.log(percent);
			// var width = (200 / 100 * percent);
			// var width = (133 - (133 / 100 * percent));
			
			// var width = (133 / 100 * percent);
			// $(loadbar).css({'width': width});
			// if (percent > 0){
			// 	$('.imgPreloader').show();
			// }
			// console.log('Upload progress: ' + percent + '% ' + message);
		},
		onFinishS3Put: function(publicUrl){
			console.log(publicUrl);
			parent.find('img.pet').attr('src', '');
			setTimeout(function(){
				parent.find('img.pet').attr('src', publicUrl + '?');
			}, 500);
			// var chipNr = $(parent).find('.chipNr').text();

			$.post(
				'/uploaded',
				{publicUrl: publicUrl, chipNr: chipNr},
				function(result){
					if (result.error){
						console.log('publicUrl ajax post error: ' + result.error);
						alert('Error posting image to server: ' + result.error);
						// $('.imgPreloader').hide();
					} else if (result.success) {
						console.log('publicUrl ajax post success: ' + result.success);
						alert('The image was uploaded successfully: ' + result.success);
						// $('.imgPreloader').hide();
					}
					$('#fileInput').attr('disabled', false);
					$(e.currentTarget).attr('id', ''); // reset id to nothing
				}
			);
		},
		onError: function(status){
			console.log('Upload error: ' + status);
			alert('There was an error uploading the image: ' + status);
			$('#fileInput').attr('disabled', false);
			// $('.imgPreloader').hide();]
			$(e.currentTarget).attr('id', ''); // reset id to nothing
		}
	});

});