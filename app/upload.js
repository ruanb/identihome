/*****************************************************
Please note that this file may have been modified, changed and/or adapted from the original.
This notice serves to fulfill the requirement of the Apache 2.0 license clause 4(b): 
"You must cause any modified files to carry prominent notices stating that You changed the files."
Please see the licenses section to find the license to the original.
*****************************************************/

// START require crypto
var crypto          = require('crypto');

// load up the user model
var User            = require('../app/models/user');

/*
 * Load the S3 information from the environment variables.
 */
var AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY_ID;
var AWS_SECRET_KEY = process.env.AWS_SECRET_ACCESS_KEY;
var S3_BUCKET = process.env.S3_BUCKET;

// app/routes.js
module.exports = function(app) {
    var upload = {};

    upload.signS3 = function(fileName, mimeType, userId, folder, callback){
        var amz_headers = "x-amz-acl:public-read";
        var signature, expires;
        function generateSignature() {
            var thisTime = new Date().getTime();
            expires = Math.ceil((thisTime + 10000)/1000);
            var put_request = "PUT\n\n" + mimeType + "\n" 
                            + expires + "\n" + amz_headers 
                            + "\n/" + S3_BUCKET + "/" 
                            + userId + '/' + folder + '/'
                            + fileName;

            signature = crypto.createHmac('sha1', AWS_SECRET_KEY)
                            .update(put_request)
                            .digest('base64');
            console.log(signature);

            if (signature.indexOf('+') != -1) {
                setTimeout(function(){
                    generateSignature();
                }, 400);
            } else {
                var url = 'https://' + S3_BUCKET + '.s3.amazonaws.com/' 
                        + userId + '/' + folder + '/'+ fileName;
                var credentials = {
                    signed_request: url + "?AWSAccessKeyId=" + AWS_ACCESS_KEY 
                                    + "&Expires=" + expires + "&Signature=" 
                                    + signature,
                    url: url,
                };

                callback(credentials);
            }
        }
        generateSignature();
    }

    /*
     * Respond to GET requests to /sign_s3.
     * Upon request, return JSON containing the temporarily-signed S3 request and the
     * anticipated URL of the image.
     */
    app.get('/signS3/:fileType/:class', function(req, res){
        var userId = req.user.id;
        var fileName = req.query.s3_object_name;
        var mimeType = req.query.s3_object_type;
        var fileType = req.params.fileType;
        var fileClass = req.params.class;
        var folder = fileType + '/' + fileClass;

        upload.signS3(fileName, mimeType, userId, folder, function(credentials){
            res.write(JSON.stringify(credentials));
            res.end();
            // res.send is the equivalent of these two commands above
            // with res.write(), you can still change headers before calling
            // res.end()
        });
    });

    /*
     * Respond to POST requests to /uploaded.
     * This function needs to be completed to handle the information in 
     * a way that suits your application.
     */
    app.post('/uploaded', function(req, res){
        var userId = req.user.id;
        var chipNr = req.body.chipNr;
        var publicUrl = req.body.publicUrl;
        console.log(publicUrl);

        User.update(
            {'_id': userId, 'pets.chipNr': chipNr},
            {'pets.$.image': publicUrl},
            function(err, affected){
                if (err) {console.log(err)}
                res.send({success: affected, error: err});
            }
        );

        // update_account(username, full_name, avatar_url); // TODO: create this function
        // TODO: Return something useful or redirect
    });

}