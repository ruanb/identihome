// load up the user model
var User       		= require('../app/models/user');

// app/routes.js
module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		if (req.isAuthenticated()) {
			var loggedIn = true;
		}
		res.render('index.ejs', {loggedIn: loggedIn}); // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	// =====================================
	// RESET ===============================
	// =====================================
	// show the reset password form
	app.get('/reset', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('reset.ejs', { message: {fail: req.flash('resetFail'), success: req.flash('resetSuccess')} });
	});

	app.get('/resetPass/:token', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('resetPass.ejs', { message: {fail:req.flash('resetFail'), success: req.flash('resetSuccess')} });
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =====================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user, // get the user out of session and pass to template
			message: req.flash('registerPet')
		});
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	// =====================================
	// REGISTER PET ========================
	// =====================================
	// process the signup form
	app.post('/registerPet', isLoggedIn, function(req, res) {
		var userId = req.user.id;
		User.update(
			{'_id': userId},
			{$push: {'pets': {
				'petName': req.body.petName,
				'chipNr': req.body.chipNr,
				'breed': req.body.breed,
				'born': req.body.born,
				'vetName': req.body.vetName,
				'vetContact': req.body.vetContact,
				'conditions': req.body.conditions || 'None',
				'medication': req.body.medication || 'None',
				'dateAdded': new Date().getTime()
			}}},
			function(err, result) {
				if (err) {
					console.log(err);
					if (err.code == 11000) {
						req.flash(
							'registerPet',
							'Sorry, that chip number has already been registered with IdentiHome.'
							// + ' Please contact support if you have any queries.'
						)
					}
				};
				res.redirect('/profile');
			}
		);
	});

	app.post('/validateChipNr', function(req, res) {
		var chipNr = req.body.chipNr;

		User.findOne(
			{'pets.chipNr': chipNr},
			'pets.$.petName',
			function(err, user1){
				if (err) {console.log(err)}
				console.log(user1);
				if (user1) {
					res.send(user1);
				} else {
					res.send({"error": "No user found"});
				}
			}
		);
	});

	app.post('/searchChip', function(req, res) {
		var chipNr = req.body.chipNr;
		var name = req.body.name;
		var contact = req.body.contact;
		var clinic = req.body.clinic;

		if (!name || !contact) {
			res.send({error: 'Please fill in your name and contact details'});
			return;
		}

		User.findOne(
			{'pets.chipNr': req.body.chipNr},
			'pets.$.petName contact local.email name alternateContact',
			function(err, user1){
				if (err) {console.log(err)}

				User.update(
					{'pets.chipNr': chipNr},
					{$push: {'searchedBy': 
						{
							'name': name,
						 	'contact': contact,
						 	'clinic': clinic || 'Not specified',
						 	'chipNr': chipNr,
						 	'time': Date.now()
						}
					}},
					function(err, result){
						if (err) {console.log(err)}

						var ownerEmail = user1.local.email;
						var ownerName = user1.name;
						delete user1.local.email;
						delete user1.name;
						res.send(user1);

						// Validate if user was found and updated
						if (user1) {
							// Send email to pet owner
							var searchedMailOptions = {
								from: 'ruan.botha.live@gmail.com',
								to: ownerEmail,
								// to: 'ruan.botha.live@gmail.com',
								subject: 'ATTENTION: IdentiHome Pet Search: ' + user1.pets[0].petName,
								html: "<h4>" + 'IdentiHome Pet Search: ' + user1.pets[0].petName + "</h4><p>" 
								+ "Dear " + ownerName + ", <br><br>" + "Please note that " + name 
								+ " has just searched for your pet (<b>" + user1.pets[0].petName + "'s"
								+ "</b>) IdentiHome chip number." + "</p><p>" 
								+ "He/she provided the following details:" + "</p><p>" 
								+ "Searcher's Name: " + name + "<br>"
								+ "Searcher's contact number: " + contact + "<br>"
								+ "Clinic / Welfare: " + clinic + "<br><br>"
								+ "Feel free to contact IdentiHome admin (0764032571) " 
								+ "if you have any further queries about this search."
								+ "<br><br>" + "Kind regards, " + "<br>" + "IdentiHome Staff"
								+ "<br>" + "(0764032571)"
							};
							sendmail(searchedMailOptions);
						}

					}
				);
			}
		);
		
	});

	app.post('/reset', function(req, res) {
		var email = req.body.email;
		var tokenTime = Date.now() + (60 * 60 * 1000);
		var token = generateToken();
		var resetUrl = req.protocol + '://' + req.headers.host + '/resetPass/' + token;

		// console.log(resetUrl);
		// return;
		var resetPasswordMail = {
		    from: 'info@vetsbrands.co.za',
		    to: email,
		    // to: 'ruan.botha.live@gmail.com',
		    subject: 'ATTENTION: IdentiHome password reset request',
		    html: 'Dear Sir/Madam <br><br>' 
		    + 'Please note that a password reset has been requested for your IdentiHome account. <br>'
		    + 'Please click the following link to reset your password: <br><br>'
		    + '<a href="' + resetUrl + '">' + resetUrl + '</a><br><br>'
		    + 'If you did not request a password request, please feel free to ignore this email' 
		    + 'or contact the IdentiHome staff at . <br><br>'
		    + 'Kind regards <br>'
		    + 'IdentiHome Staff'
		};

		User.update(
			{'local.email': email},
			{'local.token': token, 'local.tokenTime': tokenTime},
			function(err, affected){
				if (err) {
					console.log(err);
				} else if (affected == 0) {
					req.flash('resetFail', 'That email was not found in our system.')
				} else {
					// send mail with reset password token in URL
					sendmail(resetPasswordMail)
					req.flash(
						'resetSuccess',
						'A password reset request has been sent to your email. '
						+ 'Please check your email for the link to reset you password.'
					)
				}
				res.redirect('/reset');
			}
		);
	});

app.post('/resetPass/:token', function(req, res) {
	var password = req.body.password;
	var token = req.params.token;

	console.log(token);
	// return;

	User.findOne(
		{'local.token': token, 'local.tokenTime': {$gt: Date.now()}},
		function(err, user){
			if (err) {console.log(err);}
			if (!user) {
				req.flash('resetFail', 'Password reset token is invalid or has expired.');
				return res.redirect('back');
			}

			user.local.password = User().generateHash(password);
			// console.log(user.local.password);
			// return;
			user.local.token = undefined;
			user.local.tokenTime = undefined;

			user.save(function(err){
				req.login(user, function(err){
					res.redirect('/profile');
				});
			});
		}
	)
});

};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}
