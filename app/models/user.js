// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({

    local            : {
        email        : {type: String, unique: true, sparse: true},
        password     : String,
        token        : String,
        tokenTime    : Number,
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },

    name             : String,
    contact          : String,
    address          : String,
    postalCode       : String,
    alternateContact : String,
    registered       : Number,

    // pets             : Array,
    pets             : [ {
        petName: {type: String},
        chipNr: {type: mongoose.Schema.Types.Mixed, unique: true, sparse: true},
        breed: {type: String},
        born: {type: String},
        vetName: {type: String},
        vetContact: {type: String},
        conditions: {type: String},
        medication: {type: String},
        dateAdded: {type: Number},
        image: {type: String}
    }],

    promotions       : Boolean,

    searchedBy       : Array,
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// generate token to reset password
generateToken = function() {
    var buf = new Buffer(16);
    for (var i = 0; i < buf.length; i++) {
        buf[i] = Math.floor(Math.random() * 256);
    }
    var id = buf.toString('base64');
    if (id.match('/') && id.match('/').length > 0) {
        console.log(id);
        console.log('"/" forward slash in generated token - will be removed to ensure working URL compatibility');
    }
    id = id.replace('/', '');
    return id;
}

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);

// create a unique, sparse index for pets.chipNr
// userSchema.path('pets.chipNr').index({ unique: true, sparse: true });
mongoose.model('User', userSchema).ensureIndexes(function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('ensureIndexes enforced on userSchema');
    }
});